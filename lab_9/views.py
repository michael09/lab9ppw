from django.http import JsonResponse
from django.shortcuts import render
import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
# Create your views here.


def index(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        if not 'book' in request.session.keys():
            request.session['book'] = [id]
            size = 1
        else:
            books = request.session['book']
            books.append(id)
            request.session['book'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': id})
    if 'book' in request.session.keys():
        listbookSession = request.session['book']
    else:
        listbookSession = []
    nama = request.session['name']
    return render(request, 'lab_9/home.html', {'count': len(listbookSession), 'nama': nama})


def getData(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    if request.method == "POST":
        query = request.POST['query']
        url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    get = requests.get(url).json()
    hasil = get['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        if dataId in request.session['book']:
            bool = True
        else:
            bool = False
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist})


def delete(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        books = request.session['book']
        books.remove(id)
        size = len(books)
        request.session['book'] = books
        return JsonResponse({'count': size})
