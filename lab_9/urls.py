from django.urls import path
from lab_9.views import  delete, getData

urlpatterns = [
    path('delete', delete),
    path('getdata', getData),
]