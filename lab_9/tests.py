from importlib import import_module

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import requests
from django.conf import settings
# Create your tests here.
class SessionTestCase(TestCase):
    def setUp(self):
        # http://code.djangoproject.com/ticket/10899
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

class Lab9UnitTest(SessionTestCase):

    def test_lab9_not_logged_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_lab9_logged_landing_url_is_exist(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s.save()
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_login_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


    def test_client_can_POST_and_render(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s.save()
        response_post = self.client.post('/', {'id': 'abcd'})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_delete_and_delete_selected(self):
        title = "hehehe"
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['book'] = [title]
        s.save()
        response_post = self.client.post('/lab9/delete', {'id': title})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_get_data(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['book'] = []
        s.save()
        response = self.client.get('/lab9/getdata')
        self.assertEqual(response.status_code, 200)

    def test_books_already_favorit(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['book'] = []
        s.save()
        get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
        items = get['items']
        book = items[0]
        dataId = book['id']
        title = book['volumeInfo']['title']
        self.client.post('/', {'id': dataId})
        response = self.client.get('/lab9/getdata').json()
        bool = response['data'][0]['boolean']
        self.assertTrue(bool)

    def test_books_not_favorited(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['book'] = []
        s.save()
        response = self.client.get('/lab9/getdata').json()
        bool = response['data'][0]['boolean']
        self.assertFalse(bool)
