
function onSignIn(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
        sendToken(id_token);
        console.log("masuk")
    }
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }

    var sendToken = function(token) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/login",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {id_token: token },
            success: function(result) {
                console.log("signed in");
                if (result.status === "0") {
                    window.location.replace(result.url)
                } else {
                    html = "<h3>Something error, please report</h3>";
                    $("h3").replaceWith(html)
                }
            },
            error: function (error) {
                alert("Something error, please report")
            }
        })
    };