from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
# Create your views here.


def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), "667593983760-kou8i2agrjcjberoiaf8nrs9qgskl9ec.apps.googleusercontent.com")
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = idinfo['sub']
            email = idinfo['email']
            name = idinfo['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", 'url': reverse("home")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'lab_11/login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))
