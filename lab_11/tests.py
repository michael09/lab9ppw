from django.test import TestCase
from django.test import Client
# Create your tests here.

class Lab11UnitTest(TestCase):

    def test_lab11_not_logged_landing_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_lab11_false_token(self):
        response = Client().post('/login', {'id_token': "testtttttttt"}).json()
        self.assertEqual(response['status'], "1")

    def test_lab11_right_token(self):
        response = Client().post('/login', {'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjY3NTkzOTgzNzYwLWtvdThpMmFncmpjamJlcm9pYWY4bnJzOXFnc2tsOWVjLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjY3NTkzOTgzNzYwLWtvdThpMmFncmpjamJlcm9pYWY4bnJzOXFnc2tsOWVjLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTExMTg5NTA1MDkxNDgxNjc5OTk1IiwiZW1haWwiOiJoYWxpbW1pY2hhZWwwOUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IjhhZlQyaEVpQS1lS0xCZW5WT1NaSkEiLCJuYW1lIjoibWljaGFlbCBoYWxpbSIsInBpY3R1cmUiOiJodHRwczovL2xoNi5nb29nbGV1c2VyY29udGVudC5jb20vLU13cE9DVl9BVUgwL0FBQUFBQUFBQUFJL0FBQUFBQUFBQVdFL2R0bUEyR2JGR3A0L3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJtaWNoYWVsIiwiZmFtaWx5X25hbWUiOiJoYWxpbSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTQzNDYzMzY2LCJleHAiOjE1NDM0NjY5NjYsImp0aSI6ImU3YWRlOTdlZmI3NWZkZmRlMTMyZDJjZDcyYTU3OTkwOGQ4ZDA1NzUifQ.VpnlwCLE323uzoeJO6IBcBf80QD9kq07-oy60d3HxA5kFMr3BTGvsGwyyQNGkgSdR610gQCI0jlszUgv9s97A24WvIb4WV2nc-eqQVxzPkk8AZ3SoXcstO8QBT79V0EroqtzxVVWoomjy_uubliKC6cfIwHUkA7C6sSbYsW6ETryyLtEDN7zHgn939ykBUaWzFRyVG_juBgpvwkMzyhJUkqa2MsZdQRDdJiHYpAFrDJ9qSr-jG0eE68bJb7DcyIWrMobGLPVhQFz5_QPpOPlpy-WKgVhB_w1cbO0PvOWGM441r_3Bisj8Qi4ITIa7qyc1_F1WgW65UgB2k4OLxxI0Q"}).json()
        self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 302)